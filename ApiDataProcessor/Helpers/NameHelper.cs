﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ApiDataProcessor.Helpers
{
    public class NameHelper
    {
        public const string MaleNames = ".male-names.txt";
        public const string FemaleNames = ".female-names.txt";
        public const string Surnames = ".surnames.txt";

        public static IList<string> GetNamesList(string resourceFileName)
        {
            var assembly = typeof(NameHelper).Assembly;

            var nameResourceFile = assembly.GetManifestResourceNames()
                .FirstOrDefault(r => r.EndsWith(resourceFileName, StringComparison.OrdinalIgnoreCase));
            if (nameResourceFile == null)
            {
                throw new Exception($"Can't find name file {resourceFileName}");
            }

            using (var content = assembly.GetManifestResourceStream(nameResourceFile))
            using (var sr = new StreamReader(content))
            {
                var x = sr.ReadToEnd();
                return x.Split((char)10).ToList();
            }
        }
    }
}