﻿using System;
using System.IO;
using System.Linq;
using ApiDataProcessor.Models;

namespace ApiDataProcessor.Helpers
{
    public class ImageHelper
    {
        public static ImageModel GetSubjectImage()
        {
            return GetImage("SubjectImage.jpg");
        }

        private static ImageModel GetImage(string imageName)
        {
            var assembly = typeof(ImageHelper).Assembly;
            var image = assembly.GetManifestResourceNames().FirstOrDefault(r => r.EndsWith(imageName));
            if (image == null)
            {
                throw new Exception($"Can't find image {imageName}");
            }

            byte[] imageBytes;
            using (var content = assembly.GetManifestResourceStream(image))
            using (var ms = new MemoryStream())
            {
                content?.CopyTo(ms);
                imageBytes = ms.ToArray();
            }
            return new ImageModel(imageBytes, imageName);
        }
    }
}