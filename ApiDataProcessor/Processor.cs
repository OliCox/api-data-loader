﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ApiDataProcessor.EntityGeneration;
using ApiDataProcessor.Helpers;
using ApiDataProcessor.Models;
using RestSharp;
using RestSharp.Deserializers;

namespace ApiDataProcessor
{
    public class Processor
    {
        private const string OutputTemlate = "Organizations: {0} of {1}\r\nWatchlists: {2} of {3}\r\nSubjects: {4} of {5}\r\nSubject failures: {6}";

        private readonly RestClient _client;
        private readonly JsonDeserializer _jsonDeserializer;

        private const string DefaultOrganizationId = "75a13922-1379-49ce-8ad4-f252921ce386";
        private const string DefaultOrganizationName = "defaultorganization_admin";

        private readonly bool _outputToConsole;
        private readonly int _organizationCount;
        private readonly int _watchlistCount;
        private readonly int _subjectCount;
        private readonly bool _postSubjectsInParallel;
        private readonly int _totalWatchlistCount;
        private readonly int _totalSubjectCount;
        private readonly bool _addTopology;

        private int _orgsRunningTotal;
        private int _watchlistRunningTotal;
        private int _subjectRunningTotal;
        private object _singleSubjectImageId;

        private string _bearerToken;

        private int _subjectFailures;

        public Processor(bool outputToConsole, string apiUrl, int organizationCount, int watchlistCount, int subjectCount, bool postSubjectsInParallel, bool addTopology)
        {
            _client = new RestClient(apiUrl);
            _jsonDeserializer = new JsonDeserializer();

            Login();

            _outputToConsole = outputToConsole;
            _organizationCount = organizationCount;
            _watchlistCount = watchlistCount;
            _subjectCount = subjectCount;
            _postSubjectsInParallel = postSubjectsInParallel;
            _totalWatchlistCount = (_organizationCount * _watchlistCount);
            _totalSubjectCount = (_organizationCount * _watchlistCount * _subjectCount);
            _addTopology = addTopology;
        }

        private object SingleSubjectImageId
        {
            get
            {
                if (_singleSubjectImageId != null) return _singleSubjectImageId;
                _singleSubjectImageId = PostImage();
                return _singleSubjectImageId;
            }
        }

        public void AddTopology()
        {
            // 2 sites -  10 locations with 50 cameras each (total of 500 cameras per site)
            for (var si = 0; si < 2; si++)
            {
                var siteId = PostEntity(Site.NewSite(), "topology/sites", "siteId");
                for (var li = 0; li < 10; li++)
                {
                    var locationId = PostEntity(Location.NewLocation(siteId), "topology/locations", "locationId");
                    for (var ci = 0; ci < 50; ci++)
                    {
                        PostEntity(Camera.NewCamera(locationId), "topology/cameras", "cameraId");
                    }
                }
            }
        }

        public void Process()
        {
            if(_addTopology)
                AddTopology();

            OutputToConsole(_orgsRunningTotal, _watchlistRunningTotal, _subjectRunningTotal);

            // If no org count then simply target the default organization only!
            if (_organizationCount == 0)
            {
                for (var watch = 0; watch < _watchlistCount; watch++)
                {
                    var watchlistId = PostEntity(Watchlist.NewWatchlist(DefaultOrganizationId), "watchlists", "watchlistId");
                    _watchlistRunningTotal += 1;
                    OutputToConsole(_orgsRunningTotal, _watchlistRunningTotal, _subjectRunningTotal);

                    if (_postSubjectsInParallel)
                    {
                        PostSubjectsInParallel(watchlistId);
                    }
                    else
                    {
                        PostSubjects(watchlistId);
                    }

                    OutputToConsole(_orgsRunningTotal, _watchlistRunningTotal, _subjectRunningTotal);
                }
                return;
            }

            for (var org = 0; org < _organizationCount; org++)
            {
                dynamic newOrg = Organization.NewOrganization();
                var orgId = PostEntity(newOrg, "organizations", "organizationId");
                _orgsRunningTotal += 1;
                OutputToConsole(_orgsRunningTotal, _watchlistRunningTotal, _subjectRunningTotal);

                // Once org has been created we need to login to it so we can add specifics etc....
                Login(orgId.ToString(), newOrg.name);
                for (var watch = 0; watch < _watchlistCount; watch++)
                {
                    var watchlistId = PostEntity(Watchlist.NewWatchlist(orgId), "watchlists", "watchlistId");
                    _watchlistRunningTotal += 1;
                    OutputToConsole(_orgsRunningTotal, _watchlistRunningTotal, _subjectRunningTotal);

                    if (_postSubjectsInParallel)
                    {
                        PostSubjectsInParallel(watchlistId);
                    }
                    else
                    {
                        PostSubjects(watchlistId);
                    }

                    OutputToConsole(_orgsRunningTotal, _watchlistRunningTotal, _subjectRunningTotal);
                }
            }
        }

        private void PostSubjects(object watchlistId)
        {
            for (var counter = 0; counter < _subjectCount; counter++)
            {
                try
                {
                    // var subject = Subject.NewSubject(watchlistId, SingleSubjectImageId);
                    var imageId = PostImage();
                    var subject = Subject.NewSubject(watchlistId, imageId);
                    PostEntity(subject, "subjects", "subjectId");
                    _subjectRunningTotal ++;
                }
                catch
                {
                    // Don't throw exceptions. Just let it continue & storefailure count.
                    _subjectFailures ++;
                }

                OutputToConsole(_orgsRunningTotal, _watchlistRunningTotal, _subjectRunningTotal);
            }
        }

        private void PostSubjectsInParallel(object watchlistId)
        {
            long counter = 0;

            while (counter < _subjectCount)
            {
                // Batch qty in between UI updates.
                const int batchQty = 50;

                Parallel.For(
                    0,
                    batchQty,
                    index =>
                    {
                        if (Interlocked.Read(ref counter) >= _subjectCount) return;
                        Interlocked.Increment(ref counter);

                        try
                        {
                            //var subject = Subject.NewSubject(watchlistId, SingleSubjectImageId);
                            var imageId = PostImage();
                            var subject = Subject.NewSubject(watchlistId, imageId);
                            PostEntity(subject, "subjects", "subjectId");
                            Interlocked.Increment(ref _subjectRunningTotal);
                        }
                        catch
                        {
                            // Don't throw exceptions. Just let it continue & storefailure count.
                            Interlocked.Increment(ref _subjectFailures);
                        }
                    });
                OutputToConsole(_orgsRunningTotal, _watchlistRunningTotal, _subjectRunningTotal);
            }
        }

        private object PostEntity(object entity, string apiRoute, string entityIdField)
        { 
            var postRequest = CreatePostRequest(apiRoute, entity);
            try
            {
                var postResponse = PostRequest(postRequest);
                if(!string.IsNullOrEmpty(postResponse.ErrorMessage))
                {
                    throw new Exception(postResponse.ErrorMessage);
                }
                var deserialized = _jsonDeserializer.Deserialize<dynamic>(postResponse);
                var id = deserialized[entityIdField];

                return id;
            }
            catch (Exception ex)
            {         
                throw new Exception($"An error occured for POST to {apiRoute}:{Environment.NewLine}{ex.Message}");
            }
        }

        private object PostImage()
        {
            var image = ImageHelper.GetSubjectImage();
            return PostImage(image, "images");
        }

        private object PostImage(ImageModel image, string apiRoute)
        {
            var postRequest = CreateImagePostRequest(apiRoute, image);
            try
            {
                var postResponse = PostRequest(postRequest);
                if (!string.IsNullOrEmpty(postResponse.ErrorMessage))
                {
                    throw new Exception(postResponse.ErrorMessage);
                }
                var deserialized = _jsonDeserializer.Deserialize<dynamic>(postResponse);
                var id = deserialized["imageId"];
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured for POST to {apiRoute}:{Environment.NewLine}{ex.Message}");
            }
        }

        private RestRequest CreatePostRequest(string apiRoute, object entity)
        {
            var postRequest = new RestRequest(apiRoute, Method.POST) {RequestFormat = DataFormat.Json};
            postRequest.AddJsonBody(entity);
            return postRequest;
        }

        private RestRequest CreateImagePostRequest(string apiRoute, ImageModel image)
        {
            var postRequest = new RestRequest(apiRoute, Method.POST);
            postRequest.AddUrlSegment("image", image.ImageFile);
            postRequest.AddFile("image", image.ImageBytes, image.ImageFile, image.ImageType);
            return postRequest;
        }

        // Uses default orgid as created by db migration scripts in the first instance.
        private void Login(string organizationId = null, string organizationName = null)
        {
            // Using default org id as created as part of initial migration.
            var orgId = string.IsNullOrWhiteSpace(organizationId) ? DefaultOrganizationId : organizationId;
            var userName = string.IsNullOrWhiteSpace(organizationName)
                ? DefaultOrganizationName
                : string.Concat(organizationName.ToLower(), "_admin");
            var password = "system";
             
            var loginRequest = new RestRequest("login", Method.POST);
            loginRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
            loginRequest.AddHeader("cache-control", "no-cache");
            loginRequest.AddParameter(
                "application/x-www-form-urlencoded", 
                $"UserName={userName}&Password={password}&OrganizationId={orgId}&Persist=true", 
                ParameterType.RequestBody);

            try
            {
                var response = _client.Execute(loginRequest);
                var deserialized = _jsonDeserializer.Deserialize<dynamic>(response);

                // Based on expiry, looks to be 4-5days?
                _bearerToken = deserialized["access_token"];
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to login: {ex.Message}", ex);
            }      
        }

        private IRestResponse PostRequest(RestRequest request)
        {
            // Add bearer token to requests so we have valid login info
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", $"Bearer {_bearerToken}");
            return _client.Execute(request);
        }

        private void OutputToConsole(int orgCounter, int watchlistCounter, int subjectCounter)
        {
            if (!_outputToConsole) return;
            Console.Clear();
            Console.Write(
                OutputTemlate,
                orgCounter, _organizationCount,
                watchlistCounter, _totalWatchlistCount,
                subjectCounter, _totalSubjectCount,
                _subjectFailures);
        }
    }
}