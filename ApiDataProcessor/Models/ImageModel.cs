﻿using System;

namespace ApiDataProcessor.Models
{
    public class ImageModel
    {
        public ImageModel(byte[] imageBytes, string imageFile)
        {
            ImageBytes = imageBytes;
            ImageFile = imageFile;           
        }

        public byte[] ImageBytes { get; }

        public string ImageFile { get; }

        public string ImageType
        {
            get
            {
                var extensionType = ImageFile.Substring(ImageFile.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();
                switch (extensionType)
                {
                    case "jpg":
                        return "image/jpeg";
                    default:
                        return $"image/{extensionType}";
                }
            }
        }
    }
}