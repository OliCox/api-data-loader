﻿using System;
using RestSharp.Extensions;

namespace ApiDataProcessor.EntityGeneration
{
    public class Site
    {
        public static object NewSite()
        {
            var guid = Guid.NewGuid().ToString().RemoveUnderscoresAndDashes();
            var site = new
            {
                name = $"Site_{guid}",
                description = $"Site Description: {guid}",
            };
            return site;
        }
    }
}