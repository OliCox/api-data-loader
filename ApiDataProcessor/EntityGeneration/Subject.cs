﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ApiDataProcessor.Helpers;
using RestSharp.Extensions;

namespace ApiDataProcessor.EntityGeneration
{
    public class Subject
    {
        private static readonly Random Random = new Random();
        private static readonly IList<string> MaleNames = NameHelper.GetNamesList(NameHelper.MaleNames);
        private static readonly IList<string> FemaleNames = NameHelper.GetNamesList(NameHelper.FemaleNames);
        private static readonly IList<string> Surnames = NameHelper.GetNamesList(NameHelper.Surnames);
        private static readonly TextInfo TextInfo = new CultureInfo("en-US", false).TextInfo;

        public static object NewSubject(object watchlistid, object imageid)
        {
            var isMale = Convert.ToBoolean(Random.Next(2));

            var subject = new
            {
                watchlistid,
                imageid,
                title = isMale ? "Mr" : "Mrs",
                firstname = ParseName(GetRandomFirstName(isMale)),
                middlename = ParseName(GetRandomMiddleName(isMale)),
                lastname = ParseName(GetRandomSurname()),
                notes = "No_spaces_allowed_in_notes",
            };

            return subject;
        }

        private static string GetRandomFirstName(bool isMale)
        {
            try
            {
                return isMale ? MaleNames[Random.Next(MaleNames.Count)] : FemaleNames[Random.Next(FemaleNames.Count)];
            }
            catch
            {
                // Any problem with names lists means we just return a random value.
                return $"FirstName_{Guid.NewGuid()}";
            }         
        }

        private static string GetRandomMiddleName(bool isMale)
        {
            try
            {
                return isMale ? MaleNames[Random.Next(MaleNames.Count)] : FemaleNames[Random.Next(FemaleNames.Count)];
            }
            catch
            {
                // Any problem with names lists means we just return a random value.
                return $"MiddleName_{Guid.NewGuid()}";
            }
        }

        private static string GetRandomSurname()
        {
            try
            {
                return Surnames[Random.Next(Surnames.Count)];
            }
            catch
            {
                // Any problem with names lists means we just return a random value.
                return $"Lastname_{Guid.NewGuid()}";
            }
        }

        private static string ParseName(string value)
        {
            // This is work around for current validation in subjects where very restrictive e.g. no spaces, so can't have surname such as De Beers.
            return TextInfo.ToTitleCase(value).RemoveUnderscoresAndDashes().Replace(" ", string.Empty);
        }
    }
}