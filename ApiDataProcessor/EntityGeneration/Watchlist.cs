﻿using System;
using RestSharp.Extensions;

namespace ApiDataProcessor.EntityGeneration
{
    public class Watchlist
    {
        public static object NewWatchlist(object organizationId)
        {
            var guid = Guid.NewGuid().ToString().RemoveUnderscoresAndDashes();
            var watchlist = new
            {
                name = $"Watchlist_{guid}",
                description = $"Watchlist Description: {guid}",
                organizationId
            };
            return watchlist;
        }
    }
}