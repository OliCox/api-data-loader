﻿using System;
using RestSharp.Extensions;

namespace ApiDataProcessor.EntityGeneration
{
    public class Location
    {
        public static object NewLocation(object siteId)
        {
            var guid = Guid.NewGuid().ToString().RemoveUnderscoresAndDashes();
            var location = new
            {
                name = $"Location_{guid}",
                description = $"Location Description: {guid}",
                siteId
            };
            return location;
        }
    }
}