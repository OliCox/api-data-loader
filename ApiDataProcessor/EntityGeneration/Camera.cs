﻿using System;
using RestSharp.Extensions;

namespace ApiDataProcessor.EntityGeneration
{
    public class Camera
    {
        public static object NewCamera(object locationId)
        {
            var guid = Guid.NewGuid().ToString().RemoveUnderscoresAndDashes();
            var camera = new
            {
                name = $"Camera_{guid}",
                description = $"Camera Description: {guid}",
                ipAddress = "172.24.5.162",
                serverid = Guid.Parse("3d860bfa-7c24-11e6-883d-040ed9b22a7a"),
                cameraTypeId = Guid.Parse("3d85bde0-7c24-11e6-b580-96ce873eed66"),
                locationId
            };
            return camera;
        }
    }
}