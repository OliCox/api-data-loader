﻿using System;
using RestSharp.Extensions;

namespace ApiDataProcessor.EntityGeneration
{
    public class Organization
    {
        public static object NewOrganization()
        {
            var guid = Guid.NewGuid().ToString().RemoveUnderscoresAndDashes();

            // N.B. Name will be used for keyspace so ensure it is in valid format.
            var org = new
            {           
                name = $"Org{guid}",
                description = $"Org Description: {guid}",
            };

            return org;
        }
    }
}