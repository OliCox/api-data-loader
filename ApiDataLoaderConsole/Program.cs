﻿using System;
using System.Diagnostics;
using ApiDataProcessor;

namespace ApiDataLoaderConsole
{
    internal class Program
    {
        private const bool OutputToConsole = true;

        private static string _apiUrl;
        private static int _organizationCount;
        private static int _watchlistCount;
        private static int _subjectCount;
        private static bool _postSubjectsInParallel;
        private static bool _addTopology;

        private static void Main(string[] args)
        {
            if (PromptUserForValues())
            {
                var startTime = DateTime.Now;
                var stopWatch = new Stopwatch();
                stopWatch.Start();

                var result = RunProcesses();

                stopWatch.Stop();

                if (result)
                {
                    Console.WriteLine(string.Empty);
                    Console.WriteLine($"Started: {startTime.ToString("F")}");
                    Console.WriteLine($"Finished: {DateTime.Now.ToString("F")}");
                    Console.WriteLine($"Total processing time: {stopWatch.Elapsed.Hours} hours, {stopWatch.Elapsed.Minutes} minutes, {stopWatch.Elapsed.Seconds} seconds.");

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(string.Empty);
                    Console.WriteLine("Complete");
                    Console.WriteLine("Press any key to exit.");
                }
            }
            Console.ReadKey();
        }

        private static bool PromptUserForValues()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Please ensure API is up and running first of all, then......");
            Console.WriteLine("Press any key to supply setup values.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Please enter API url - http://localhost:9000/api is default if not supplied.");
            Console.ForegroundColor = ConsoleColor.Yellow;
            _apiUrl = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(_apiUrl))
            {
                _apiUrl = "http://localhost:9000/api";
                Console.WriteLine(_apiUrl);
                Console.WriteLine(string.Empty);
            }
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Do you want to add Topology - Y/N?");
            Console.ForegroundColor = ConsoleColor.Yellow;
            _addTopology = string.Equals(Console.ReadLine(), "Y", StringComparison.OrdinalIgnoreCase);
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("How many organizations do you want to create?");
            Console.ForegroundColor = ConsoleColor.Yellow;
            if (!int.TryParse(Console.ReadLine(), out _organizationCount))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid value - integer is expected");
                Console.ReadLine();
                return false;
            }
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(
                _organizationCount == 0 ? 
                "How many watchlists do you want to create for the default organization?" 
                : "How many watchlists do you want to create per organization?");
            Console.ForegroundColor = ConsoleColor.Yellow;
            if (!int.TryParse(Console.ReadLine(), out _watchlistCount))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid value - integer is expected");
                Console.ReadLine();
                return false;
            }
            Console.ForegroundColor = ConsoleColor.White;

            if (_watchlistCount != 0)
            {
                Console.WriteLine("How many subjects do you want to create per watchlist?");
                Console.ForegroundColor = ConsoleColor.Yellow;
                if (!int.TryParse(Console.ReadLine(), out _subjectCount))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid value - integer is expected");
                    Console.ReadLine();
                    return false;
                }
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine("Post subjects in parallel - Y/N?");
                Console.ForegroundColor = ConsoleColor.Yellow;
                _postSubjectsInParallel = !string.Equals(Console.ReadLine(), "N", StringComparison.OrdinalIgnoreCase);
                Console.ForegroundColor = ConsoleColor.White;
            }

            Console.WriteLine(string.Empty);
            Console.WriteLine(
                $"In total you will create {_organizationCount} organizations, " +
                $"{(Math.Max(_organizationCount, 1)*_watchlistCount)} watchlists" +
                $" & {(Math.Max(_organizationCount,1) * Math.Max(_watchlistCount,1) *_subjectCount)} subjects. Press any key to start processing!");
            Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            return true;
        }

        private static bool RunProcesses()
        {
            try
            {
                var processor = new Processor(
                    OutputToConsole,
                    _apiUrl, 
                    _organizationCount, 
                    _watchlistCount,
                    _subjectCount, 
                    _postSubjectsInParallel, 
                    _addTopology);
                processor.Process();
                return true;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Empty);
                Console.WriteLine($"Error: {ex.Message}");
                return false;
            }
        }
    }
}